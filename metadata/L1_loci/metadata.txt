"File_name" "Tissue" "Antibody" "ReplicateChar" "Experiment" "Experiment_label" "BufferNum" "BufferChar" "Inj_Vol" "Replicate" "Cell_type" "Inj_vol" "analyzed" "Drug"
"162N_aORF1_1" "162N" "Orf1" "a" "62N_aORF1_1" "162N_Orf1_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"162N_aORF1_2" "162N" "Orf1" "b" "62N_aORF1_2" "162N_Orf1_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"162N_aORF1_3" "162N" "Orf1" "c" "62N_aORF1_3" "162N_Orf1_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"162N_aORF1_4" "162N" "Orf1" "d" "62N_aORF1_4" "162N_Orf1_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"162N_aORF1_buffer_1_Repeat" "162N" "Orf1" "repeat" "62N_aORF1_buffer_1_Repeat" "162N_Orf1_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"162N_aORF1_5" "162N" "Orf1" "a" "62N_aORF1_5" "162N_Orf1_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"162N_aORF1_6" "162N" "Orf1" "b" "62N_aORF1_6" "162N_Orf1_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"162N_aORF1_7" "162N" "Orf1" "c" "62N_aORF1_7" "162N_Orf1_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"162N_aORF1_8" "162N" "Orf1" "d" "62N_aORF1_8" "162N_Orf1_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"162N_aORF1_10" "162N" "Orf1" "b" "62N_aORF1_10" "162N_Orf1_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"162N_aORF1_11" "162N" "Orf1" "c" "62N_aORF1_11" "162N_Orf1_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"162N_aORF1_12" "162N" "Orf1" "d" "62N_aORF1_12" "162N_Orf1_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"162N_aORF1_9" "162N" "Orf1" "a" "62N_aORF1_9" "162N_Orf1_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"162N_aORF1_13" "162N" "Orf1" "a" "62N_aORF1_13" "162N_Orf1_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"162N_aORF1_14" "162N" "Orf1" "b" "62N_aORF1_14" "162N_Orf1_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"162N_aORF1_15" "162N" "Orf1" "c" "62N_aORF1_15" "162N_Orf1_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"162N_aORF1_16" "162N" "Orf1" "d" "62N_aORF1_16" "162N_Orf1_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"162N_aORF1_17" "162N" "Orf1" "a" "62N_aORF1_17" "162N_Orf1_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"162N_aORF1_18" "162N" "Orf1" "b" "62N_aORF1_18" "162N_Orf1_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"162N_aORF1_19" "162N" "Orf1" "c" "62N_aORF1_19" "162N_Orf1_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"162N_aORF1_20" "162N" "Orf1" "d" "62N_aORF1_20" "162N_Orf1_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"162N_aORF1_21" "162N" "Orf1" "a" "62N_aORF1_21" "162N_Orf1_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"162N_aORF1_22" "162N" "Orf1" "b" "62N_aORF1_22" "162N_Orf1_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"162N_aORF1_23" "162N" "Orf1" "c" "62N_aORF1_23" "162N_Orf1_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"162N_aORF1_24" "162N" "Orf1" "d" "62N_aORF1_24" "162N_Orf1_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"162T_IgG_1" "162T" "IgG" "a" "62T_IgG_1" "162T_IgG_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"162T_IgG_2" "162T" "IgG" "b" "62T_IgG_2" "162T_IgG_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"162T_IgG_3" "162T" "IgG" "c" "62T_IgG_3" "162T_IgG_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"162T_IgG_4" "162T" "IgG" "d" "62T_IgG_4" "162T_IgG_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"162T_IgG_5" "162T" "IgG" "a" "62T_IgG_5" "162T_IgG_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"162T_IgG_6" "162T" "IgG" "b" "62T_IgG_6" "162T_IgG_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"162T_IgG_7" "162T" "IgG" "c" "62T_IgG_7" "162T_IgG_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"162T_IgG_8" "162T" "IgG" "d" "62T_IgG_8" "162T_IgG_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"162T_IgG_10" "162T" "IgG" "b" "62T_IgG_10" "162T_IgG_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"162T_IgG_11" "162T" "IgG" "c" "62T_IgG_11" "162T_IgG_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"162T_IgG_12" "162T" "IgG" "d" "62T_IgG_12" "162T_IgG_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"162T_IgG_9" "162T" "IgG" "a" "62T_IgG_9" "162T_IgG_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"162T_IgG_13" "162T" "IgG" "a" "62T_IgG_13" "162T_IgG_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"162T_IgG_14" "162T" "IgG" "b" "62T_IgG_14" "162T_IgG_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"162T_IgG_15" "162T" "IgG" "c" "62T_IgG_15" "162T_IgG_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"162T_IgG_16" "162T" "IgG" "d" "62T_IgG_16" "162T_IgG_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"162T_IgG_17" "162T" "IgG" "a" "62T_IgG_17" "162T_IgG_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"162T_IgG_18" "162T" "IgG" "b" "62T_IgG_18" "162T_IgG_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"162T_IgG_19" "162T" "IgG" "c" "62T_IgG_19" "162T_IgG_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"162T_IgG_20" "162T" "IgG" "d" "62T_IgG_20" "162T_IgG_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"162T_IgG_21" "162T" "IgG" "a" "62T_IgG_21" "162T_IgG_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"162T_IgG_22" "162T" "IgG" "b" "62T_IgG_22" "162T_IgG_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"162T_IgG_23" "162T" "IgG" "c" "62T_IgG_23" "162T_IgG_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"162T_IgG_24" "162T" "IgG" "d" "62T_IgG_24" "162T_IgG_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"162T_aORF1_1" "162T" "Orf1" "a" "62T_aORF1_1" "162T_Orf1_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"162T_aORF1_2" "162T" "Orf1" "b" "62T_aORF1_2" "162T_Orf1_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"162T_aORF1_3" "162T" "Orf1" "c" "62T_aORF1_3" "162T_Orf1_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"162T_aORF1_4" "162T" "Orf1" "d" "62T_aORF1_4" "162T_Orf1_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"162T_aORF1_5" "162T" "Orf1" "a" "62T_aORF1_5" "162T_Orf1_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"162T_aORF1_6" "162T" "Orf1" "b" "62T_aORF1_6" "162T_Orf1_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"162T_aORF1_7" "162T" "Orf1" "c" "62T_aORF1_7" "162T_Orf1_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"162T_aORF1_8" "162T" "Orf1" "d" "62T_aORF1_8" "162T_Orf1_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"162T_aORF1_10" "162T" "Orf1" "b" "62T_aORF1_10" "162T_Orf1_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"162T_aORF1_11" "162T" "Orf1" "c" "62T_aORF1_11" "162T_Orf1_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"162T_aORF1_12" "162T" "Orf1" "d" "62T_aORF1_12" "162T_Orf1_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"162T_aORF1_9" "162T" "Orf1" "a" "62T_aORF1_9" "162T_Orf1_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"162T_aORF1_13" "162T" "Orf1" "a" "62T_aORF1_13" "162T_Orf1_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"162T_aORF1_14" "162T" "Orf1" "b" "62T_aORF1_14" "162T_Orf1_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"162T_aORF1_15" "162T" "Orf1" "c" "62T_aORF1_15" "162T_Orf1_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"162T_aORF1_16" "162T" "Orf1" "d" "62T_aORF1_16" "162T_Orf1_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"162T_aORF1_17" "162T" "Orf1" "a" "62T_aORF1_17" "162T_Orf1_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"162T_aORF1_18" "162T" "Orf1" "b" "62T_aORF1_18" "162T_Orf1_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"162T_aORF1_19" "162T" "Orf1" "c" "62T_aORF1_19" "162T_Orf1_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"162T_aORF1_20" "162T" "Orf1" "d" "62T_aORF1_20" "162T_Orf1_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"162T_aORF1_21" "162T" "Orf1" "a" "62T_aORF1_21" "162T_Orf1_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"162T_aORF1_22" "162T" "Orf1" "b" "62T_aORF1_22" "162T_Orf1_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"162T_aORF1_23" "162T" "Orf1" "c" "62T_aORF1_23" "162T_Orf1_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"162T_aORF1_24" "162T" "Orf1" "d" "62T_aORF1_24" "162T_Orf1_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"197N_aORF1_1" "197N" "Orf1" "a" "97N_aORF1_1" "197N_Orf1_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"197N_aORF1_2" "197N" "Orf1" "b" "97N_aORF1_2" "197N_Orf1_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"197N_aORF1_3" "197N" "Orf1" "c" "97N_aORF1_3" "197N_Orf1_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"197N_aORF1_4" "197N" "Orf1" "d" "97N_aORF1_4" "197N_Orf1_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"197N_aORF1_5" "197N" "Orf1" "a" "97N_aORF1_5" "197N_Orf1_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"197N_aORF1_6" "197N" "Orf1" "b" "97N_aORF1_6" "197N_Orf1_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"197N_aORF1_7" "197N" "Orf1" "c" "97N_aORF1_7" "197N_Orf1_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"197N_aORF1_8" "197N" "Orf1" "d" "97N_aORF1_8" "197N_Orf1_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"197N_aORF1_10" "197N" "Orf1" "b" "97N_aORF1_10" "197N_Orf1_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"197N_aORF1_11" "197N" "Orf1" "c" "97N_aORF1_11" "197N_Orf1_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"197N_aORF1_12" "197N" "Orf1" "d" "97N_aORF1_12" "197N_Orf1_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"197N_aORF1_9" "197N" "Orf1" "a" "97N_aORF1_9" "197N_Orf1_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"197N_aORF1_13" "197N" "Orf1" "a" "97N_aORF1_13" "197N_Orf1_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"197N_aORF1_14" "197N" "Orf1" "b" "97N_aORF1_14" "197N_Orf1_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"197N_aORF1_15" "197N" "Orf1" "c" "97N_aORF1_15" "197N_Orf1_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"197N_aORF1_16" "197N" "Orf1" "d" "97N_aORF1_16" "197N_Orf1_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"197N_aORF1_17" "197N" "Orf1" "a" "97N_aORF1_17" "197N_Orf1_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"197N_aORF1_18" "197N" "Orf1" "b" "97N_aORF1_18" "197N_Orf1_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"197N_aORF1_19" "197N" "Orf1" "c" "97N_aORF1_19" "197N_Orf1_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"197N_aORF1_20" "197N" "Orf1" "d" "97N_aORF1_20" "197N_Orf1_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"197N_aORF1_21" "197N" "Orf1" "a" "97N_aORF1_21" "197N_Orf1_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"197N_aORF1_22" "197N" "Orf1" "b" "97N_aORF1_22" "197N_Orf1_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"197N_aORF1_23" "197N" "Orf1" "c" "97N_aORF1_23" "197N_Orf1_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"197N_aORF1_24" "197N" "Orf1" "d" "97N_aORF1_24" "197N_Orf1_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"197T_IgG_1" "197T" "IgG" "a" "97T_IgG_1" "197T_IgG_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"197T_IgG_2" "197T" "IgG" "b" "97T_IgG_2" "197T_IgG_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"197T_IgG_3" "197T" "IgG" "c" "97T_IgG_3" "197T_IgG_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"197T_IgG_4" "197T" "IgG" "d" "97T_IgG_4" "197T_IgG_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"197T_IgG_5" "197T" "IgG" "a" "97T_IgG_5" "197T_IgG_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"197T_IgG_6" "197T" "IgG" "b" "97T_IgG_6" "197T_IgG_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"197T_IgG_7" "197T" "IgG" "c" "97T_IgG_7" "197T_IgG_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"197T_IgG_8" "197T" "IgG" "d" "97T_IgG_8" "197T_IgG_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"197T_IgG_10" "197T" "IgG" "b" "97T_IgG_10" "197T_IgG_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"197T_IgG_11" "197T" "IgG" "c" "97T_IgG_11" "197T_IgG_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"197T_IgG_12" "197T" "IgG" "d" "97T_IgG_12" "197T_IgG_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"197T_IgG_9" "197T" "IgG" "a" "97T_IgG_9" "197T_IgG_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"197T_IgG_buffer_10_Rep_a" "197T" "IgG" "repeat_a" "97T_IgG_buffer_10_Rep_a" "197T_IgG_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"197T_IgG_buffer_10_Rep_b" "197T" "IgG" "repeat_b" "97T_IgG_buffer_10_Rep_b" "197T_IgG_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"197T_IgG_13" "197T" "IgG" "a" "97T_IgG_13" "197T_IgG_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"197T_IgG_14" "197T" "IgG" "b" "97T_IgG_14" "197T_IgG_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"197T_IgG_15" "197T" "IgG" "c" "97T_IgG_15" "197T_IgG_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"197T_IgG_16" "197T" "IgG" "d" "97T_IgG_16" "197T_IgG_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"197T_IgG_17" "197T" "IgG" "a" "97T_IgG_17" "197T_IgG_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"197T_IgG_18" "197T" "IgG" "b" "97T_IgG_18" "197T_IgG_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"197T_IgG_19" "197T" "IgG" "c" "97T_IgG_19" "197T_IgG_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"197T_IgG_20" "197T" "IgG" "d" "97T_IgG_20" "197T_IgG_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"197T_IgG_21" "197T" "IgG" "a" "97T_IgG_21" "197T_IgG_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"197T_IgG_22" "197T" "IgG" "b" "97T_IgG_22" "197T_IgG_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"197T_IgG_23" "197T" "IgG" "c" "97T_IgG_23" "197T_IgG_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"197T_IgG_24" "197T" "IgG" "d" "97T_IgG_24" "197T_IgG_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"197T_IgG_buffer_17_Rep_a" "197T" "IgG" "repeat_a" "97T_IgG_buffer_17_Rep_a" "197T_IgG_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"197T_IgG_buffer_17_Rep_b" "197T" "IgG" "repeat_b" "97T_IgG_buffer_17_Rep_b" "197T_IgG_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"197T_aORF1_1" "197T" "Orf1" "a" "97T_aORF1_1" "197T_Orf1_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"197T_aORF1_2" "197T" "Orf1" "b" "97T_aORF1_2" "197T_Orf1_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"197T_aORF1_3" "197T" "Orf1" "c" "97T_aORF1_3" "197T_Orf1_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"197T_aORF1_4" "197T" "Orf1" "d" "97T_aORF1_4" "197T_Orf1_Buffer1" 1 "1_NH4Acet_NaCl" 5 NA NA NA NA NA
"197T_aORF1_5" "197T" "Orf1" "a" "97T_aORF1_5" "197T_Orf1_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"197T_aORF1_6" "197T" "Orf1" "b" "97T_aORF1_6" "197T_Orf1_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"197T_aORF1_7" "197T" "Orf1" "c" "97T_aORF1_7" "197T_Orf1_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"197T_aORF1_8" "197T" "Orf1" "d" "97T_aORF1_8" "197T_Orf1_Buffer9" 9 "9_HEPESNa_NaCl" 5 NA NA NA NA NA
"197T_aORF1_10" "197T" "Orf1" "b" "97T_aORF1_10" "197T_Orf1_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"197T_aORF1_11" "197T" "Orf1" "c" "97T_aORF1_11" "197T_Orf1_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"197T_aORF1_12" "197T" "Orf1" "d" "97T_aORF1_12" "197T_Orf1_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"197T_aORF1_9" "197T" "Orf1" "a" "97T_aORF1_9" "197T_Orf1_Buffer10" 10 "10_HEPESNa_MgCl2" 5 NA NA NA NA NA
"197T_aORF1_13" "197T" "Orf1" "a" "97T_aORF1_13" "197T_Orf1_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"197T_aORF1_14" "197T" "Orf1" "b" "97T_aORF1_14" "197T_Orf1_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"197T_aORF1_15" "197T" "Orf1" "c" "97T_aORF1_15" "197T_Orf1_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"197T_aORF1_16" "197T" "Orf1" "d" "97T_aORF1_16" "197T_Orf1_Buffer11" 11 "11_HEPESNA_MgCl2" 5 NA NA NA NA NA
"197T_aORF1_17" "197T" "Orf1" "a" "97T_aORF1_17" "197T_Orf1_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"197T_aORF1_18" "197T" "Orf1" "b" "97T_aORF1_18" "197T_Orf1_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"197T_aORF1_19" "197T" "Orf1" "c" "97T_aORF1_19" "197T_Orf1_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"197T_aORF1_20" "197T" "Orf1" "d" "97T_aORF1_20" "197T_Orf1_Buffer13" 13 "13_HEPESK_KAcet" 5 NA NA NA NA NA
"197T_aORF1_21" "197T" "Orf1" "a" "97T_aORF1_21" "197T_Orf1_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"197T_aORF1_22" "197T" "Orf1" "b" "97T_aORF1_22" "197T_Orf1_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"197T_aORF1_23" "197T" "Orf1" "c" "97T_aORF1_23" "197T_Orf1_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"197T_aORF1_24" "197T" "Orf1" "d" "97T_aORF1_24" "197T_Orf1_Buffer17" 17 "17_TRISCl_Na3Cit" 5 NA NA NA NA NA
"144T_Orf1_1" "144T_A" "Orf1" NA "144T_ORF1_9a" "144T_A_Orf1" NA NA NA 1 NA NA NA NA
"144T_Orf1_2" "144T_A" "Orf1" NA "144T_ORF1_9b" "144T_A_Orf1" NA NA NA 2 NA NA NA NA
"144T_Orf1_3" "144T_A" "Orf1" NA "144T_ORF1_9c" "144T_A_Orf1" NA NA NA 3 NA NA NA NA
"144T_mIgG1" "144T_A" "mIgG" NA "144T_IgG_10a" "144T_A_mIgG" NA NA NA 1 NA NA NA NA
"144T_mIgG2" "144T_A" "mIgG" NA "144T_IgG_10b" "144T_A_mIgG" NA NA NA 2 NA NA NA NA
"144T_mIgG3" "144T_A" "mIgG" NA "144T_IgG_10c" "144T_A_mIgG" NA NA NA 3 NA NA NA NA
"20170427_144T_1a_170427152838" "144T_B" "Orf1" NA "144T_ORF1_1a" "144T_B_Orf1" NA NA NA 1 NA NA NA NA
"20170427_144T_1b" "144T_B" "Orf1" NA "144T_ORF1_1b" "144T_B_Orf1" NA NA NA 2 NA NA NA NA
"20170427_144T_1c" "144T_B" "Orf1" NA "144T_ORF1_1c" "144T_B_Orf1" NA NA NA 3 NA NA NA NA
"20170427_144T_2a" "144T_B" "mIgG" NA "144T_IgG_2a" "144T_B_mIgG" NA NA NA 1 NA NA NA NA
"20170427_144T_2b" "144T_B" "mIgG" NA "144T_IgG_2b" "144T_B_mIgG" NA NA NA 2 NA NA NA NA
"20170427_144T_2c" "144T_B" "mIgG" NA "144T_IgG_2c" "144T_B_mIgG" NA NA NA 3 NA NA NA NA
"20170427_159N_5a" "159N" "Orf1" NA "159N_ORF1_5a" "159N_Orf1" NA NA NA 1 NA NA NA NA
"20170427_159N_5b" "159N" "Orf1" NA "159N_ORF1_5b" "159N_Orf1" NA NA NA 2 NA NA NA NA
"20170427_159N_5c" "159N" "Orf1" NA "159N_ORF1_5c" "159N_Orf1" NA NA NA 3 NA NA NA NA
"20170427_159T_3a" "159T" "Orf1" NA "159T_ORF1_3a" "159T_Orf1" NA NA NA 1 NA NA NA NA
"20170427_159T_3b" "159T" "Orf1" NA "159T_ORF1_3b" "159T_Orf1" NA NA NA 2 NA NA NA NA
"20170427_159T_3c" "159T" "Orf1" NA "159T_ORF1_3c" "159T_Orf1" NA NA NA 3 NA NA NA NA
"20170427_159T_4a" "159T" "mIgG" NA "159T_IgG_4a" "159T_mIgG" NA NA NA 1 NA NA NA NA
"20170427_159T_4b" "159T" "mIgG" NA "159T_IgG_4b" "159T_mIgG" NA NA NA 2 NA NA NA NA
"20170427_159T_4c" "159T" "mIgG" NA "159T_IgG_4c" "159T_mIgG" NA NA NA 3 NA NA NA NA
"20170427_163N_8a" "163N" "Orf1" NA "163N_ORF1_8a" "163N_Orf1" NA NA NA 1 NA NA NA NA
"20170427_163N_8b" "163N" "Orf1" NA "163N_ORF1_8b" "163N_Orf1" NA NA NA 2 NA NA NA NA
"20170427_163N_8c" "163N" "Orf1" NA "163N_ORF1_8c" "163N_Orf1" NA NA NA 3 NA NA NA NA
"20170427_163T_6a" "163T" "Orf1" NA "163T_ORF1_6a" "163T_Orf1" NA NA NA 1 NA NA NA NA
"20170427_163T_6b" "163T" "Orf1" NA "163T_ORF1_6b" "163T_Orf1" NA NA NA 2 NA NA NA NA
"20170427_163T_6c" "163T" "Orf1" NA "163T_ORF1_6c" "163T_Orf1" NA NA NA 3 NA NA NA NA
"20170427_163T_7a" "163T" "mIgG" NA "163T_IgG_7a" "163T_mIgG" NA NA NA 1 NA NA NA NA
"20170427_163T_7b" "163T" "mIgG" NA "163T_IgG_7b" "163T_mIgG" NA NA NA 2 NA NA NA NA
"20170427_163T_7c" "163T" "mIgG" NA "163T_IgG_7c" "163T_mIgG" NA NA NA 3 NA NA NA NA
"Hua_1_second_exp_N2102ep_IgG_5uL" NA "IgG" NA "1_second_N2102ep_5uL_IgG" "N2102ep_IgG_direct_extraction" NA NA NA 1 "N2102ep" "5uL" "x" NA
"Hua_2_second_exp_N2102ep_IgG_5uL" NA "IgG" NA "2_second_N2102ep_5uL_IgG" "N2102ep_IgG_direct_extraction" NA NA NA 2 "N2102ep" "5uL" "x" NA
"Hua_3_second_exp_N2102ep_IgG_5uL" NA "IgG" NA "3_second_N2102ep_5uL_IgG" "N2102ep_IgG_direct_extraction" NA NA NA 3 "N2102ep" "5uL" "x" NA
"Hua_4_second_exp_N2102ep_IgG_5uL" NA "IgG" NA "4_second_N2102ep_5uL_IgG" "N2102ep_IgG_direct_extraction" NA NA NA 4 "N2102ep" "5uL" "x" NA
"Hua_5_second_exp_NTERA2_IgG_5uL" NA "IgG" NA "5_second_NTERA2_5uL_IgG" "NTERA2_IgG_direct_extraction" NA NA NA 5 "NTERA2" "5uL" "x" NA
"Hua_6_second_exp_NTERA2_IgG_5uL" NA "IgG" NA "6_second_NTERA2_5uL_IgG" "NTERA2_IgG_direct_extraction" NA NA NA 6 "NTERA2" "5uL" "x" NA
"Hua_7_second_exp_NTERA2_IgG_5uL" NA "IgG" NA "7_second_NTERA2_5uL_IgG" "NTERA2_IgG_direct_extraction" NA NA NA 7 "NTERA2" "5uL" "x" NA
"Hua_8_second_exp_NTERA2_IgG_5uL" NA "IgG" NA "8_second_NTERA2_5uL_IgG" "NTERA2_IgG_direct_extraction" NA NA NA 8 "NTERA2" "5uL" "x" NA
"Hua_9_second_exp_PA1PC_IgG_5uL" NA "IgG" NA "9_second_PA1PC_5uL_IgG" "PA1PC_IgG_direct_extraction" NA NA NA 9 "PA1PC" "5uL" "x" NA
"Hua_10_second_exp_PA1PC_IgG_5uL" NA "IgG" NA "10_second_PA1PC_5uL_IgG" "PA1PC_IgG_direct_extraction" NA NA NA 10 "PA1PC" "5uL" "x" NA
"Hua_11_second_exp_PA1PC_IgG_5uL" NA "IgG" NA "11_second_PA1PC_5uL_IgG" "PA1PC_IgG_direct_extraction" NA NA NA 11 "PA1PC" "5uL" "x" NA
"Hua_12_second_exp_PA1PC_IgG_5uL" NA "IgG" NA "12_second_PA1PC_5uL_IgG" "PA1PC_IgG_direct_extraction" NA NA NA 12 "PA1PC" "5uL" "x" NA
"Hua_5_N2102ep_IgG_1" NA "mIgG" NA "1_N2102ep_Rabbit_IgG" "N2102ep_mIgG_direct_extraction" NA NA NA 5 "N2102ep" "5uL" "x" NA
"Hua_6_N2102ep_IgG_2" NA "mIgG" NA "2_N2102ep_Rabbit_IgG" "N2102ep_mIgG_direct_extraction" NA NA NA 6 "N2102ep" "5uL" "x" NA
"Hua_7_N2102ep_IgG_3" NA "mIgG" NA "3_N2102ep_Rabbit_IgG" "N2102ep_mIgG_direct_extraction" NA NA NA 7 "N2102ep" "5uL" "x" NA
"Hua_8_N2102ep_IgG_4" NA "mIgG" NA "4_N2102ep_Rabbit_IgG" "N2102ep_mIgG_direct_extraction" NA NA NA 8 "N2102ep" "5uL" "x" NA
"Hua_1_second_exp_N2102ep_Orf1_5uL" NA "Orf1" NA "1_second_N2102ep_5uL_ORF1" "N2102ep_Orf1_direct_extraction" NA NA NA 1 "N2102ep" "5uL" "x" NA
"Hua_2_second_exp_N2102ep_Orf1_5uL" NA "Orf1" NA "2_second_N2102ep_5uL_ORF1" "N2102ep_Orf1_direct_extraction" NA NA NA 2 "N2102ep" "5uL" "x" NA
"Hua_3_second_exp_N2102ep_Orf1_5uL" NA "Orf1" NA "3_second_N2102ep_5uL_ORF1" "N2102ep_Orf1_direct_extraction" NA NA NA 3 "N2102ep" "5uL" "x" NA
"Hua_4_second_exp_N2102ep_Orf1_5uL" NA "Orf1" NA "4_second_N2102ep_5uL_ORF1" "N2102ep_Orf1_direct_extraction" NA NA NA 4 "N2102ep" "5uL" "x" NA
"Hua_5_second_exp_NTERA2_Orf1_5uL" NA "Orf1" NA "5_second_NTERA2_5uL_ORF1" "NTERA2_Orf1_direct_extraction" NA NA NA 5 "NTERA2" "5uL" "x" NA
"Hua_6_second_exp_NTERA2_Orf1_5uL" NA "Orf1" NA "6_second_NTERA2_5uL_ORF1" "NTERA2_Orf1_direct_extraction" NA NA NA 6 "NTERA2" "5uL" "x" NA
"Hua_7_second_exp_NTERA2_Orf1_5uL" NA "Orf1" NA "7_second_NTERA2_5uL_ORF1" "NTERA2_Orf1_direct_extraction" NA NA NA 7 "NTERA2" "5uL" "x" NA
"Hua_8_second_exp_NTERA2_Orf1_5uL" NA "Orf1" NA "8_second_NTERA2_5uL_ORF1" "NTERA2_Orf1_direct_extraction" NA NA NA 8 "NTERA2" "5uL" "x" NA
"Hua_9_second_exp_PA1PC_Orf1_5uL_20201009175613" NA "Orf1" NA "9_second_PA1PC_5uL_ORF1" "PA1PC_Orf1_direct_extraction" NA NA NA 9 "PA1PC" "5uL" "x" NA
"Hua_10_second_exp_PA1PC_Orf1_5uL_20201009190438" NA "Orf1" NA "10_second_PA1PC_5uL_ORF1" "PA1PC_Orf1_direct_extraction" NA NA NA 10 "PA1PC" "5uL" "x" NA
"Hua_11_second_exp_PA1PC_Orf1_5uL" NA "Orf1" NA "11_second_PA1PC_5uL_ORF1" "PA1PC_Orf1_direct_extraction" NA NA NA 11 "PA1PC" "5uL" "x" NA
"Hua_12_second_exp_PA1PC_Orf1_5uL" NA "Orf1" NA "12_second_PA1PC_5uL_ORF1" "PA1PC_Orf1_direct_extraction" NA NA NA 12 "PA1PC" "5uL" "x" NA
"Hua_1_N2102ep_Orf2_1" NA "Orf2" NA "4_N2102ep_5uL_ORF2" "N2102ep_Orf2_direct_extraction" NA NA NA 1 "N2102ep" "5uL" "x" NA
"Hua_2_N2102ep_Orf2_2" NA "Orf2" NA "3_N2102ep_5uL_ORF2" "N2102ep_Orf2_direct_extraction" NA NA NA 2 "N2102ep" "5uL" "x" NA
"Hua_3_N2102ep_Orf2_3" NA "Orf2" NA "2_N2102ep_5uL_ORF2" "N2102ep_Orf2_direct_extraction" NA NA NA 3 "N2102ep" "5uL" "x" NA
"Hua_4_N2102ep_Orf2_4" NA "Orf2" NA "1_N2102ep_5uL_ORF2" "N2102ep_Orf2_direct_extraction" NA NA NA 4 "N2102ep" "5uL" "x" NA
"sample_37_buffer10_dmso_rep1" NA NA NA "37_buffer10_dmso_rep1" "dmso_buffer10" 10 "20mM HEPES Na pH7.4, 50mM?MgCl, 0.1% Tween 20" 5 1 "N2102EP" NA NA "dmso"
"sample_38_buffer10_dmso_rep2" NA NA NA "38_buffer10_dmso_rep2" "dmso_buffer10" 10 "20mM HEPES Na pH7.4, 50mM?MgCl, 0.1% Tween 20" 5 2 "N2102EP" NA NA "dmso"
"sample_39_buffer10_dmso_rep3" NA NA NA "39_buffer10_dmso_rep3" "dmso_buffer10" 10 "20mM HEPES Na pH7.4, 50mM?MgCl, 0.1% Tween 20" 5 3 "N2102EP" NA NA "dmso"
"sample_40_buffer10_dmso_rep4" NA NA NA "40_buffer10_dmso_rep4" "dmso_buffer10" 10 "20mM HEPES Na pH7.4, 50mM?MgCl, 0.1% Tween 20" 5 4 "N2102EP" NA NA "dmso"
"sample_5_buffer13_dmso_rep1" NA NA NA "5_buffer13_dmso_rep1" "dmso_buffer13" 13 "20mM HEPES K pH 7.4, 300mM KAcet, 0.1% Tween 20" 5 1 "N2102EP" NA NA "dmso"
"sample_6_buffer13_dmso_rep2" NA NA NA "6_buffer13_dmso_rep2" "dmso_buffer13" 13 "20mM HEPES K pH 7.4, 300mM KAcet, 0.1% Tween 20" 5 2 "N2102EP" NA NA "dmso"
"sample_7_buffer13_dmso_rep3" NA NA NA "7_buffer13_dmso_rep3" "dmso_buffer13" 13 "20mM HEPES K pH 7.4, 300mM KAcet, 0.1% Tween 20" 5 3 "N2102EP" NA NA "dmso"
"sample_8_buffer13_dmso_rep4" NA NA NA "8_buffer13_dmso_rep4" "dmso_buffer13" 13 "20mM HEPES K pH 7.4, 300mM KAcet, 0.1% Tween 20" 5 4 "N2102EP" NA NA "dmso"
"sample_53_buffer17_dmso_rep1" NA NA NA "53_buffer17_dmso_rep1" "dmso_buffer17" 17 "20mM?TrisCl?pH 8.0, 300mM Na3Cit, 1% Triton X100" 5 1 "N2102EP" NA NA "dmso"
"sample_54_buffer17_dmso_rep2" NA NA NA "54_buffer17_dmso_rep2" "dmso_buffer17" 17 "20mM?TrisCl?pH 8.0, 300mM Na3Cit, 1% Triton X100" 5 2 "N2102EP" NA NA "dmso"
"sample_55_buffer17_dmso_rep3" NA NA NA "55_buffer17_dmso_rep3" "dmso_buffer17" 17 "20mM?TrisCl?pH 8.0, 300mM Na3Cit, 1% Triton X100" 5 3 "N2102EP" NA NA "dmso"
"sample_56_buffer17_dmso_rep4" NA NA NA "56_buffer17_dmso_rep4" "dmso_buffer17" 17 "20mM?TrisCl?pH 8.0, 300mM Na3Cit, 1% Triton X100" 5 4 "N2102EP" NA NA "dmso"
"sample_21_l1extractionbuffer_dmso_rep1" NA NA NA "21_l1extractionbuffer_dmso_rep1" "dmso_buffer7" 7 "20mM HEPES Na pH7.4, 500mM?NaCl, 1% Triton X-100" 5 1 "N2102EP" NA NA "dmso"
"sample_22_l1extractionbuffer_dmso_rep2" NA NA NA "22_l1extractionbuffer_dmso_rep2" "dmso_buffer7" 7 "20mM HEPES Na pH7.4, 500mM?NaCl, 1% Triton X-100" 5 2 "N2102EP" NA NA "dmso"
"sample_23_l1extractionbuffer_dmso_rep3" NA NA NA "23_l1extractionbuffer_dmso_rep3" "dmso_buffer7" 7 "20mM HEPES Na pH7.4, 500mM?NaCl, 1% Triton X-100" 5 3 "N2102EP" NA NA "dmso"
"sample_24_l1extractionbuffer_dmso_rep4" NA NA NA "24_l1extractionbuffer_dmso_rep4" "dmso_buffer7" 7 "20mM HEPES Na pH7.4, 500mM?NaCl, 1% Triton X-100" 5 4 "N2102EP" NA NA "dmso"
"sample_45_buffer10_drug42_rep1" NA NA NA "45_buffer10_drug42_rep1" "drug12_buffer10" 10 "20mM HEPES Na pH7.4, 50mM?MgCl, 0.1% Tween 20" 5 1 "N2102EP" NA NA "drug12"
"sample_46_buffer10_drug42_rep2" NA NA NA "46_buffer10_drug42_rep2" "drug12_buffer10" 10 "20mM HEPES Na pH7.4, 50mM?MgCl, 0.1% Tween 20" 5 2 "N2102EP" NA NA "drug12"
"sample_47_buffer10_drug42_rep3" NA NA NA "47_buffer10_drug42_rep3" "drug12_buffer10" 10 "20mM HEPES Na pH7.4, 50mM?MgCl, 0.1% Tween 20" 5 3 "N2102EP" NA NA "drug12"
"sample_48_buffer10_drug42_rep4" NA NA NA "48_buffer10_drug42_rep4" "drug12_buffer10" 10 "20mM HEPES Na pH7.4, 50mM?MgCl, 0.1% Tween 20" 5 4 "N2102EP" NA NA "drug12"
"sample_13_buffer13_drug42_rep1" NA NA NA "13_buffer13_drug42_rep1" "drug12_buffer13" 13 "20mM HEPES K pH 7.4, 300mM KAcet, 0.1% Tween 20" 5 1 "N2102EP" NA NA "drug12"
"sample_14_buffer13_drug42_rep2" NA NA NA "14_buffer13_drug42_rep2" "drug12_buffer13" 13 "20mM HEPES K pH 7.4, 300mM KAcet, 0.1% Tween 20" 5 2 "N2102EP" NA NA "drug12"
"sample_15_buffer13_drug42_rep3" NA NA NA "15_buffer13_drug42_rep3" "drug12_buffer13" 13 "20mM HEPES K pH 7.4, 300mM KAcet, 0.1% Tween 20" 5 3 "N2102EP" NA NA "drug12"
"sample_16_buffer13_drug42_rep4" NA NA NA "16_buffer13_drug42_rep4" "drug12_buffer13" 13 "20mM HEPES K pH 7.4, 300mM KAcet, 0.1% Tween 20" 5 4 "N2102EP" NA NA "drug12"
"sample_61_lbuffer17_drug42_rep1" NA NA NA "61_lbuffer17_drug42_rep1" "drug12_buffer17" 17 "20mM?TrisCl?pH 8.0, 300mM Na3Cit, 1% Triton X100" 5 1 "N2102EP" NA NA "drug12"
"sample_62_buffer17_drug42_rep2" NA NA NA "62_buffer17_drug42_rep2" "drug12_buffer17" 17 "20mM?TrisCl?pH 8.0, 300mM Na3Cit, 1% Triton X100" 5 2 "N2102EP" NA NA "drug12"
"sample_63_buffer17_drug42_rep3" NA NA NA "63_buffer17_drug42_rep3" "drug12_buffer17" 17 "20mM?TrisCl?pH 8.0, 300mM Na3Cit, 1% Triton X100" 5 3 "N2102EP" NA NA "drug12"
"sample_64_buffer17_drug42_rep4" NA NA NA "64_buffer17_drug42_rep4" "drug12_buffer17" 17 "20mM?TrisCl?pH 8.0, 300mM Na3Cit, 1% Triton X100" 5 4 "N2102EP" NA NA "drug12"
"sample_29_l1extractionbuffer_drug42_rep1" NA NA NA "29_l1extractionbuffer_drug42_rep1" "drug12_buffer7" 7 "20mM HEPES Na pH7.4, 500mM?NaCl, 1% Triton X-100" 5 1 "N2102EP" NA NA "drug12"
"sample_30_l1extractionbuffer_drug42_rep2" NA NA NA "30_l1extractionbuffer_drug42_rep2" "drug12_buffer7" 7 "20mM HEPES Na pH7.4, 500mM?NaCl, 1% Triton X-100" 5 2 "N2102EP" NA NA "drug12"
"sample_31_l1extractionbuffer_drug42_rep3" NA NA NA "31_l1extractionbuffer_drug42_rep3" "drug12_buffer7" 7 "20mM HEPES Na pH7.4, 500mM?NaCl, 1% Triton X-100" 5 3 "N2102EP" NA NA "drug12"
"sample_32_l1extractionbuffer_drug42_rep4" NA NA NA "32_l1extractionbuffer_drug42_rep4" "drug12_buffer7" 7 "20mM HEPES Na pH7.4, 500mM?NaCl, 1% Triton X-100" 5 4 "N2102EP" NA NA "drug12"
"sample_41_buffer10_drug473_rep1" NA NA NA "41_buffer10_drug473_rep1" "drug473_buffer10" 10 "20mM HEPES Na pH7.4, 50mM?MgCl, 0.1% Tween 20" 5 1 "N2102EP" NA NA "drug473"
"sample_42_buffer10_drug473_rep2" NA NA NA "42_buffer10_drug473_rep2" "drug473_buffer10" 10 "20mM HEPES Na pH7.4, 50mM?MgCl, 0.1% Tween 20" 5 2 "N2102EP" NA NA "drug473"
"sample_43_buffer10_drug473_rep3" NA NA NA "43_buffer10_drug473_rep3" "drug473_buffer10" 10 "20mM HEPES Na pH7.4, 50mM?MgCl, 0.1% Tween 20" 5 3 "N2102EP" NA NA "drug473"
"sample_44_buffer10_drug473_rep4" NA NA NA "44_buffer10_drug473_rep4" "drug473_buffer10" 10 "20mM HEPES Na pH7.4, 50mM?MgCl, 0.1% Tween 20" 5 4 "N2102EP" NA NA "drug473"
"sample_10_buffer13_drug473_rep2" NA NA NA "10_buffer13_drug473_rep2" "drug473_buffer13" 13 "20mM HEPES K pH 7.4, 300mM KAcet, 0.1% Tween 20" 5 2 "N2102EP" NA NA "drug473"
"sample_11_buffer13_drug473_rep3" NA NA NA "11_buffer13_drug473_rep3" "drug473_buffer13" 13 "20mM HEPES K pH 7.4, 300mM KAcet, 0.1% Tween 20" 5 3 "N2102EP" NA NA "drug473"
"sample_12_buffer13_drug473_rep4" NA NA NA "12_buffer13_drug473_rep4" "drug473_buffer13" 13 "20mM HEPES K pH 7.4, 300mM KAcet, 0.1% Tween 20" 5 4 "N2102EP" NA NA "drug473"
"sample_9_buffer13_drug473_rep1" NA NA NA "9_buffer13_drug473_rep1" "drug473_buffer13" 13 "20mM HEPES K pH 7.4, 300mM KAcet, 0.1% Tween 20" 5 1 "N2102EP" NA NA "drug473"
"sample_57_buffer17_drug473_rep1" NA NA NA "57_buffer17_drug473_rep1" "drug473_buffer17" 17 "20mM?TrisCl?pH 8.0, 300mM Na3Cit, 1% Triton X100" 5 1 "N2102EP" NA NA "drug473"
"sample_58_buffer17_drug473_rep2" NA NA NA "58_buffer17_drug473_rep2" "drug473_buffer17" 17 "20mM?TrisCl?pH 8.0, 300mM Na3Cit, 1% Triton X100" 5 2 "N2102EP" NA NA "drug473"
"sample_59_buffer17_drug473_rep3" NA NA NA "59_buffer17_drug473_rep3" "drug473_buffer17" 17 "20mM?TrisCl?pH 8.0, 300mM Na3Cit, 1% Triton X100" 5 3 "N2102EP" NA NA "drug473"
"sample_60_buffer17_drug473_rep4" NA NA NA "60_buffer17_drug473_rep4" "drug473_buffer17" 17 "20mM?TrisCl?pH 8.0, 300mM Na3Cit, 1% Triton X100" 5 4 "N2102EP" NA NA "drug473"
"sample_25_l1extractionbuffer_drug473_rep1" NA NA NA "25_l1extractionbuffer_drug473_rep1" "drug473_buffer7" 7 "20mM HEPES Na pH7.4, 500mM?NaCl, 1% Triton X-100" 5 1 "N2102EP" NA NA "drug473"
"sample_26_l1extractionbuffer_drug473_rep2" NA NA NA "26_l1extractionbuffer_drug473_rep2" "drug473_buffer7" 7 "20mM HEPES Na pH7.4, 500mM?NaCl, 1% Triton X-100" 5 2 "N2102EP" NA NA "drug473"
"sample_27_l1extractionbuffer_drug473_rep3" NA NA NA "27_l1extractionbuffer_drug473_rep3" "drug473_buffer7" 7 "20mM HEPES Na pH7.4, 500mM?NaCl, 1% Triton X-100" 5 3 "N2102EP" NA NA "drug473"
"sample_28_l1extractionbuffer_drug473_rep4" NA NA NA "28_l1extractionbuffer_drug473_rep4" "drug473_buffer7" 7 "20mM HEPES Na pH7.4, 500mM?NaCl, 1% Triton X-100" 5 4 "N2102EP" NA NA "drug473"
"sample_33_buffer10_notreatment_rep1" NA NA NA "33_buffer10_notreatment_rep1" "notreatment_buffer10" 10 "20mM HEPES Na pH7.4, 50mM?MgCl, 0.1% Tween 20" 5 1 "N2102EP" NA NA "notreatment"
"sample_34_buffer10_notreatment_rep2" NA NA NA "34_buffer10_notreatment_rep2" "notreatment_buffer10" 10 "20mM HEPES Na pH7.4, 50mM?MgCl, 0.1% Tween 20" 5 2 "N2102EP" NA NA "notreatment"
"sample_35_buffer10_notreatment_rep3" NA NA NA "35_buffer10_notreatment_rep3" "notreatment_buffer10" 10 "20mM HEPES Na pH7.4, 50mM?MgCl, 0.1% Tween 20" 5 3 "N2102EP" NA NA "notreatment"
"sample_36_buffer10_notreatment_rep4" NA NA NA "36_buffer10_notreatment_rep4" "notreatment_buffer10" 10 "20mM HEPES Na pH7.4, 50mM?MgCl, 0.1% Tween 20" 5 4 "N2102EP" NA NA "notreatment"
"sample_1_buffer13_notreatment_rep1" NA NA NA "1_buffer13_notreatment_rep1" "notreatment_buffer13" 13 "20mM HEPES K pH 7.4, 300mM KAcet, 0.1% Tween 20" 5 1 "N2102EP" NA NA "notreatment"
"sample_2_buffer13_notreatment_rep2" NA NA NA "2_buffer13_notreatment_rep2" "notreatment_buffer13" 13 "20mM HEPES K pH 7.4, 300mM KAcet, 0.1% Tween 20" 5 2 "N2102EP" NA NA "notreatment"
"sample_3_buffer13_notreatment_rep3" NA NA NA "3_buffer13_notreatment_rep3" "notreatment_buffer13" 13 "20mM HEPES K pH 7.4, 300mM KAcet, 0.1% Tween 20" 5 3 "N2102EP" NA NA "notreatment"
"sample_4_buffer13_notreatment_rep4" NA NA NA "4_buffer13_notreatment_rep4" "notreatment_buffer13" 13 "20mM HEPES K pH 7.4, 300mM KAcet, 0.1% Tween 20" 5 4 "N2102EP" NA NA "notreatment"
"sample_49_buffer17_notreatment_rep1" NA NA NA "49_buffer17_notreatment_rep1" "notreatment_buffer17" 17 "20mM?TrisCl?pH 8.0, 300mM Na3Cit, 1% Triton X100" 5 1 "N2102EP" NA NA "notreatment"
"sample_50_buffer17_notreatment_rep2" NA NA NA "50_buffer17_notreatment_rep2" "notreatment_buffer17" 17 "20mM?TrisCl?pH 8.0, 300mM Na3Cit, 1% Triton X100" 5 2 "N2102EP" NA NA "notreatment"
"sample_51_buffer17_notreatment_rep3" NA NA NA "51_buffer17_notreatment_rep3" "notreatment_buffer17" 17 "20mM?TrisCl?pH 8.0, 300mM Na3Cit, 1% Triton X100" 5 3 "N2102EP" NA NA "notreatment"
"sample_52_buffer17_notreatment_rep4" NA NA NA "52_buffer17_notreatment_rep4" "notreatment_buffer17" 17 "20mM?TrisCl?pH 8.0, 300mM Na3Cit, 1% Triton X100" 5 4 "N2102EP" NA NA "notreatment"
"sample_17_l1extractionbuffer_notreatment_rep1" NA NA NA "17_l1extractionbuffer_notreatment_rep1" "notreatment_buffer7" 7 "20mM HEPES Na pH7.4, 500mM?NaCl, 1% Triton X-100" 5 1 "N2102EP" NA NA "notreatment"
"sample_18_l1extractionbuffer_notreatment_rep2" NA NA NA "18_l1extractionbuffer_notreatment_rep2" "notreatment_buffer7" 7 "20mM HEPES Na pH7.4, 500mM?NaCl, 1% Triton X-100" 5 2 "N2102EP" NA NA "notreatment"
"sample_19_l1extractionbuffer_notreatment_rep3" NA NA NA "19_l1extractionbuffer_notreatment_rep3" "notreatment_buffer7" 7 "20mM HEPES Na pH7.4, 500mM?NaCl, 1% Triton X-100" 5 3 "N2102EP" NA NA "notreatment"
"sample_20_l1extractionbuffer_notreatment_rep4" NA NA NA "20_l1extractionbuffer_notreatment_rep4" "notreatment_buffer7" 7 "20mM HEPES Na pH7.4, 500mM?NaCl, 1% Triton X-100" 5 4 "N2102EP" NA NA "notreatment"
"Screen_March_mIgG_LFQ_1" NA "mIgG" NA "mIgG_LFQ_1" "N2102Ep_mIgG_Buffer1" 1 NA NA 1 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_10" NA "mIgG" NA "mIgG_LFQ_10" "N2102Ep_mIgG_Buffer10" 10 NA NA 2 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_11" NA "mIgG" NA "mIgG_LFQ_11" "N2102Ep_mIgG_Buffer10" 10 NA NA 3 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_12" NA "mIgG" NA "mIgG_LFQ_12" "N2102Ep_mIgG_Buffer10" 10 NA NA 4 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_13" NA "mIgG" NA "mIgG_LFQ_13" "N2102Ep_mIgG_Buffer11" 11 NA NA 1 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_15" NA "mIgG" NA "mIgG_LFQ_15" "N2102Ep_mIgG_Buffer11" 11 NA NA 3 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_16" NA "mIgG" NA "mIgG_LFQ_16" "N2102Ep_mIgG_Buffer11" 11 NA NA 4 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_17" NA "mIgG" NA "mIgG_LFQ_17" "N2102Ep_mIgG_Buffer13" 13 NA NA 1 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_18" NA "mIgG" NA "mIgG_LFQ_18" "N2102Ep_mIgG_Buffer13" 13 NA NA 2 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_19" NA "mIgG" NA "mIgG_LFQ_19" "N2102Ep_mIgG_Buffer13" 13 NA NA 3 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_2" NA "mIgG" NA "mIgG_LFQ_2" "N2102Ep_mIgG_Buffer1" 1 NA NA 2 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_20" NA "mIgG" NA "mIgG_LFQ_20" "N2102Ep_mIgG_Buffer13" 13 NA NA 4 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_21" NA "mIgG" NA "mIgG_LFQ_21" "N2102Ep_mIgG_Buffer17" 17 NA NA 1 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_22" NA "mIgG" NA "mIgG_LFQ_22" "N2102Ep_mIgG_Buffer17" 17 NA NA 2 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_23" NA "mIgG" NA "mIgG_LFQ_23" "N2102Ep_mIgG_Buffer17" 17 NA NA 3 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_24" NA "mIgG" NA "mIgG_LFQ_24" "N2102Ep_mIgG_Buffer17" 17 NA NA 4 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_3" NA "mIgG" NA "mIgG_LFQ_3" "N2102Ep_mIgG_Buffer1" 1 NA NA 3 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_4" NA "mIgG" NA "mIgG_LFQ_4" "N2102Ep_mIgG_Buffer1" 1 NA NA 4 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_5" NA "mIgG" NA "mIgG_LFQ_5" "N2102Ep_mIgG_Buffer9" 9 NA NA 1 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_6" NA "mIgG" NA "mIgG_LFQ_6" "N2102Ep_mIgG_Buffer9" 9 NA NA 2 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_7" NA "mIgG" NA "mIgG_LFQ_7" "N2102Ep_mIgG_Buffer9" 9 NA NA 3 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_8" NA "mIgG" NA "mIgG_LFQ_8" "N2102Ep_mIgG_Buffer9" 9 NA NA 4 "N2102Ep" NA NA NA
"Screen_March_mIgG_LFQ_9" NA "mIgG" NA "mIgG_LFQ_9" "N2102Ep_mIgG_Buffer10" 10 NA NA 1 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_1" NA "Orf1" NA "Orf1_LFQ_1" "N2102Ep_Orf1_Buffer1" 1 NA NA 1 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_11" NA "Orf1" NA "Orf1_LFQ_11" "N2102Ep_Orf1_Buffer10" 10 NA NA 3 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_12" NA "Orf1" NA "Orf1_LFQ_12" "N2102Ep_Orf1_Buffer10" 10 NA NA 4 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_13" NA "Orf1" NA "Orf1_LFQ_13" "N2102Ep_Orf1_Buffer11" 11 NA NA 1 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_14" NA "Orf1" NA "Orf1_LFQ_14" "N2102Ep_Orf1_Buffer11" 11 NA NA 2 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_15" NA "Orf1" NA "Orf1_LFQ_15" "N2102Ep_Orf1_Buffer11" 11 NA NA 3 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_16" NA "Orf1" NA "Orf1_LFQ_16" "N2102Ep_Orf1_Buffer11" 11 NA NA 4 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_18" NA "Orf1" NA "Orf1_LFQ_18" "N2102Ep_Orf1_Buffer13" 13 NA NA 2 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_19" NA "Orf1" NA "Orf1_LFQ_19" "N2102Ep_Orf1_Buffer13" 13 NA NA 3 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_2" NA "Orf1" NA "Orf1_LFQ_2" "N2102Ep_Orf1_Buffer1" 1 NA NA 2 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_20" NA "Orf1" NA "Orf1_LFQ_20" "N2102Ep_Orf1_Buffer13" 13 NA NA 4 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_21" NA "Orf1" NA "Orf1_LFQ_21" "N2102Ep_Orf1_Buffer17" 17 NA NA 1 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_22" NA "Orf1" NA "Orf1_LFQ_22" "N2102Ep_Orf1_Buffer17" 17 NA NA 2 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_23" NA "Orf1" NA "Orf1_LFQ_23" "N2102Ep_Orf1_Buffer17" 17 NA NA 3 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_24" NA "Orf1" NA "Orf1_LFQ_24" "N2102Ep_Orf1_Buffer17" 17 NA NA 4 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_3" NA "Orf1" NA "Orf1_LFQ_3" "N2102Ep_Orf1_Buffer1" 1 NA NA 3 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_4" NA "Orf1" NA "Orf1_LFQ_4" "N2102Ep_Orf1_Buffer1" 1 NA NA 4 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_5" NA "Orf1" NA "Orf1_LFQ_5" "N2102Ep_Orf1_Buffer9" 9 NA NA 1 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_6" NA "Orf1" NA "Orf1_LFQ_6" "N2102Ep_Orf1_Buffer9" 9 NA NA 2 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_7" NA "Orf1" NA "Orf1_LFQ_7" "N2102Ep_Orf1_Buffer9" 9 NA NA 3 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_8" NA "Orf1" NA "Orf1_LFQ_8" "N2102Ep_Orf1_Buffer9" 9 NA NA 4 "N2102Ep" NA NA NA
"Screen_March_Orf1_LFQ_9" NA "Orf1" NA "Orf1_LFQ_9" "N2102Ep_Orf1_Buffer10" 10 NA NA 1 "N2102Ep" NA NA NA
